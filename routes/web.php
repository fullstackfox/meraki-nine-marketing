<?php

Route::get('/', 'PageController@home');
Route::patch('/contents/{content}', 'ContentsController@update');

Route::post('/admin/content', 'ContentsController@store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
