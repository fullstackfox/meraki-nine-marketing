<?php

namespace Tests\Feature;

use App\Content;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ManageDataTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    function can_edit_data_in_a_section()
    {
        $this->withoutExceptionHandling();

        $section = factory(Content::class)->create([
            'section_name' => 'Section',
            'slug' => 'section',
            'content' => [
                'header' => 'old header',
                'footer' => 'old footer',
            ]
        ]);

        $response = $this->patch("/contents/{$section->slug}", [
            'section_name' => 'Section',
            'slug' => 'section',
            'content' => [
                'header' => 'new header',
                'footer' => 'new footer',
            ],
        ]);

        $this->assertDatabaseHas('contents', [
            'section_name' => 'Section'
        ])->assertDatabaseHas('contents', [
            'content' => json_encode([
                'header' => 'new header',
                'footer' => 'new footer',
            ]),
        ]);
    }
}
