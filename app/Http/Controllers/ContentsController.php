<?php

namespace App\Http\Controllers;

use App\Content;
use Illuminate\Http\Request;

class ContentsController extends Controller
{
    public function update(Content $content)
    {
        $content->update(\request()->all());

        return redirect()->back();
    }
}
