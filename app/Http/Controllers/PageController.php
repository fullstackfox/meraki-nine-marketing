<?php

namespace App\Http\Controllers;

use App\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class PageController extends Controller
{
    public function home()
    {
        $topbar = Content::where('slug', 'topbar')->firstOrFail();

        return view('welcome')->with([
            'logo' => Arr::get($topbar->content, 'logo', asset('images/logo.png')),
            'quote' => Arr::get($topbar->content, 'quote'),
            'quote_author' => Arr::get($topbar->content, 'quote_author')
        ]);
    }
}
