<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $guarded = [];

    protected $casts = [
        'content' => 'json',
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
