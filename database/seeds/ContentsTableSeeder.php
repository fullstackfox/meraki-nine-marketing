<?php

use App\Content;
use Illuminate\Database\Seeder;

class ContentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Content::create([
            'section_name' => 'Topbar',
            'slug' => 'topbar',
            'content' => [
                'logo' => asset('images/logo.png'),
                'quote' => 'Our jobs as marketers are to understand how the customer wants to buy and help them to do so.',
                'quote_author' => 'Bryan Eisenberg - Keynote Speaker, Author'
            ]
        ]);

        Content::create([
            'section_name' => 'Hero Section',
            'slug' => 'hero-section',
            'content' => [
                'header' => 'Want More Customers And Sales?',
                'small_text' => 'We Help Small Businesses Generate More Leads and Sales Using Digital Marketing',
                'button' => 'Start Here To Learn More'
            ]
        ]);

        Content::create([
            'section_name' => 'Business Name',
            'slug' => 'business-name',
            'content' => [
                'name' => 'Meraki Nine Marketing',
                'image' => asset('images/MNM_home_photo.jpg')
            ]
        ]);

        Content::create([
            'section_name' => 'Bio',
            'slug' => 'bio',
            'content' => [
                'headline' => 'Who Are We?',
                'image' => asset('images/logo'),
                'text' => 'At Meraki Nine we put our heart into everything we do. We stand on integrity and believe in honesty and good character. We put our best foot forward to bring you the results you deserve.'
            ]
        ]);

        Content::create([
            'section_name' => 'Call to Action',
            'slug' => 'call-to-action',
            'content' => [
                'headline' => 'DO YOU WANT US TO SEND YOU CUSTOMERS?',
                'text' => 'Because we have a proven system that actually delivers results, our services are in high demand. If you are considering becoming a client, please click the button below and fill out the short questionnaire and schedule a time to talk. Spots are first come first served.',
                'button' => 'Apply Now'
            ]
        ]);

        Content::create([
            'section_name' => 'What We Do',
            'slug' => 'what-we-do',
            'content' => [
                'text' => 'lots of text'
            ]
        ]);

        Content::create([
            'section_name' => 'Strategies',
            'slug' => 'strategies',
            'content' => [
                'text' => 'lots of text'
            ]
        ]);

        Content::create([
            'section_name' => 'Footer',
            'slug' => 'footer',
            'content' => [
                'logo' => asset('images/logo.png'),
                'copyright' => 'Meraki Nine Marketing - Copyright 2019',
                'link' => 'Apply Now For An Assessment Call'
            ]
        ]);
    }
}
