<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Meraki Nine Marketing</title>

        <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
    </head>
    <body class="leading-normal text-indigo-darkest">
        <div class="flex flex-col w-full">

            @include('frontend.topbar')
            @include('frontend.call-to-action')
            @include('frontend.hero')
            @include('frontend.bio')
            @include('frontend.schedule')
            @include('frontend.info')
            @include('frontend.schedule')
            @include('frontend.strategies')

            <!-- Schedule A Call -->
            <div class="bg-grey-lightest flex justify-center py-12 px-12">
                <div class="max-w-lg">
                    <p class="font-bold text-center text-lg md:text-xl"><a class="no-underline text-indigo hover:text-indigo-lighter" href="https://forms.gle/fnmfa271zzY7nXKL6">Schedule a call</a> with us to discuss the best strategies for your business to use based on your goals.</p>
                </div>
            </div>

            @include('frontend.footer')
        </div>
    </body>
</html>
