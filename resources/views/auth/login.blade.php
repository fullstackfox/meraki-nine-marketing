@extends('layouts.app')

@section('content')
        <div class="h-screen w-full flex items-center justify-center">
            <div class="container flex items-center justify-center">

                <form class="w-full max-w-xs" method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="md:flex md:items-center mb-6">
                        <div class="md:w-1/3">
                            <label class="block text-grey font-bold md:text-right mb-1 md:mb-0 pr-4" for="email">
                                {{ __('E-Mail Address') }}
                            </label>
                        </div>
                        <div class="md:w-2/3">
                            <input id="email" name="email" type="email" class="bg-grey-lighter appearance-none border-2 border-grey-lighter rounded w-full py-2 px-4 text-grey-darker leading-tight focus:outline-none focus:bg-white focus:border-purple{{ $errors->has('email') ? ' border border-red' : '' }}" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="text-sm text-grey-darker" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="md:flex md:items-center mb-6">
                        <div class="md:w-1/3">
                            <label class="block text-grey font-bold md:text-right mb-1 md:mb-0 pr-4" for="password">
                                {{ __('Password') }}
                            </label>
                        </div>
                        <div class="md:w-2/3">
                            <input class="bg-grey-lighter appearance-none border-2 border-grey-lighter rounded w-full py-2 px-4 text-grey-darker leading-tight focus:outline-none focus:bg-white focus:border-purple{{ $errors->has('password') ? ' border border-red' : '' }}" id="password" type="password" placeholder="******" name="password" required>

                            @if ($errors->has('password'))
                                <span class="text-sm text-grey-darker" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="md:flex md:items-center mb-6">
                        <div class="md:w-1/3"></div>
                        <label class="md:w-2/3 block text-grey font-bold">
                            <input class="mr-2 leading-tight" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <span class="text-sm">
                                {{ __('Remember Me') }}
                            </span>
                        </label>
                    </div>

                    <div class="md:flex md:items-center">
                        <div class="md:w-1/3"></div>
                        <div class="md:w-2/3">
                            <button type="submit" class="shadow bg-indigo hover:bg-indigo-light focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded">
                                {{ __('Login') }}
                            </button>

                            @if (Route::has('password.request'))
                                <a class="no-underline text-grey-dark" href="{{ route('password.request') }}">
                                    <br><br>{{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                        </div>
                    </div>

                </form>
            </div>
        </div>
@endsection
