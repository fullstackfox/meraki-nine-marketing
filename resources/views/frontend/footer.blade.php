<!-- Footer -->
<div class="bg-indigo-darker flex flex-wrap-reverse items-center justify-center md:justify-between py-4 px-12">
    <div class="flex items-center">
        <img class="h-12 mr-6" src="{{ asset('/images/logo-white.png') }}" alt="Meraki Nine Marketing Logo">

        <p class="text-blue-lightest">Meraki Nine Marketing - Copyright 2019</p>
    </div>

    <div class="pb-6 md:pb-0">
        <a href="https://forms.gle/fnmfa271zzY7nXKL6" class="no-underline text-blue-lightest custom-yellow-underline hover:text-yellow">Apply Now For An Assessment Call</a>
    </div>
</div>