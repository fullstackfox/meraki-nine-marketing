<!-- Bio Section -->
<div class="bg-grey-lightest flex justify-center py-24 px-12">
    <div class="container flex flex-col justify-start">
        <h1 class="text-4xl text-center md:text-left font-normal pb-12 md:w-2/5"><span class="custom-underline">Who Are We?</span></h1>

        <div class="flex flex-wrap items-center justify-around md:-mx-8">
            <img class="w-64 mt-8 mb-12 md:mt-0 md:mb-0 md:px-8" src="{{ asset('/images/logo.png') }}" alt="Meraki Nine Marketing Logo">

            <p class="md:w-3/5 text-center md:text-right font-normal text-lg leading-loose text-grey-darkest md:px-8">At Meraki Nine we put our heart into everything we do. We stand on integrity and believe in honesty and good character. We put our best foot forward to bring you the results you deserve.</p>
        </div>
    </div>
</div>