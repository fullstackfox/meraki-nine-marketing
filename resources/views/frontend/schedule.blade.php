<!-- Call To Action -->
<div class="bg-indigo-dark flex justify-center">
    <div class="container flex flex-col py-24 px-12">
        <h1 class="text-center text-blue-lightest text-3xl lg:text-4xl font-normal pb-6">DO YOU WANT US <br class="sm:hidden" />TO SEND YOU CUSTOMERS?</h1>

        <p class="text-center text-lg lg:text-xl text-blue-lightest leading-loose pb-8">Because we have a proven system that actually delivers results, our services are in high demand. If you are considering becoming a client, please click the button below and fill out the short questionnaire and schedule a time to talk. Spots are first come first served.</p>

        <div class="flex justify-center">
            <a href="https://forms.gle/fnmfa271zzY7nXKL6" class="text-center rounded-lg shadow py-3 text-indigo-darkest text-xl font-bold no-underline bg-yellow-dark w-3/5 md:w-2/5 lg:w-1/5 hover:bg-yellow">Apply Now</a>
        </div>
    </div>
</div>