<!-- Customers Now And Later Section -->
<div class="bg-grey-lightest flex justify-center pt-24 px-12">
    <div class="flex flex-col justify-start max-w-lg pb-12 border-b-2 border-indigo leading-loose md:leading-normal">

        <h1 class="text-4xl lg:text-5xl font-normal leading-normal py-12 text-center">3 Ways We Can Get You Customers Now & Customers Later</h1>

        <!-- Customers Now -->
        <h2 class="text-3xl md:text-4xl font-normal my-8"><span class="custom-underline">Customers Now</span></h2>

        <!-- Facebook Advertising -->
        <h3 class="text-2xl lg:text-4xl text-left leading-normal font-bold md:font-normal mt-8 mb-4">Facebook Advertising</h3>
        <p class="text-grey-darkest text-base md:text-xl my-2">We're not talking about '<span class="italic">Likes</span>' and '<span class="italic">Fans</span>'.</p>
        <p class="text-grey-darkest text-base md:text-xl my-2">Try to pay your bills with a '<span class="italic">Like</span>'...</p>
        <p class="text-grey-darkest text-base md:text-xl my-2"><span class="italic">Real</span> Facebook Advertising is one of the <span class="italic">best</span> ways to get actual <span class="italic">paying</span> clients and customers today.</p>
        <p class="text-grey-darkest text-base md:text-xl my-2">We also <span class="italic">track</span> everything and can tie <span class="italic">every</span> lead and sale to marketing budget spent so you can see <span class="italic custom-underline">exactly</span> what your marketing budget is doing.</p>
        <p class="text-grey-darkest text-base md:text-xl my-2">On multiple occasions we've generated leads for clients in the first <span class="italic">24 hours</span> of a campaign.</p>

        <!-- Pay Per Click -->
        <h3 class="text-2xl lg:text-4xl text-left leading-normal font-bold md:font-normal mt-8 mb-4">Pay Per Click</h3>
        <p class="text-grey-darkest text-base md:text-xl my-2">Pay Per Click advertising is one of the <span class="italic">most effective</span> marketing strategies when done <span class="italic">correctly</span>.</p>
        <p class="text-grey-darkest text-base md:text-xl my-2">We're <span class="italic custom-underline">experts</span> and use it to generate leads <span class="italic">quickly</span> who are actively searching for <span class="italic custom-underline">your</span> services and are more <span class="italic">likely</span> to turn into a client or customer.</p>
        <p class="text-grey-darkest text-base md:text-xl my-2">Like Facebook Advertising, we can <span class="italic">accurately</span> track every budget spent to determine your ROI.</p>
        <p class="text-grey-darkest text-base md:text-xl my-2">Leads can come in as <span class="italic">fast</span> as <span class="italic font-extrabold custom-underline">24 hours</span>.</p>

        <!-- Customers Later -->
        <h2 class="text-3xl md:text-4xl font-normal mt-16 mb-8"><span class="custom-underline">Customers Later</span></h2>

        <!-- Organic Traffic - SEO -->
        <h3 class="text-2xl lg:text-4xl text-left leading-normal font-bold md:font-normal mt-8 mb-4">Organic Traffic - SEO</h3>
        <p class="text-grey-darkest text-xl my-2">No marketing source delivers a <span class="italic custom-underline">higher</span> conversion rate than <span class="italic">organic</span> traffic from search engines.</p>
        <p class="text-grey-darkest text-xl my-2">But make <span class="italic">no mistake</span>, although it's <span class="italic custom-underline">highly-effective</span>, SEO can take time when it's done <span class="italic">correctly</span>.</p>
        <p class="text-grey-darkest text-xl my-2">Even though we've significantly <span class="italic">increased</span> website <span class="italic">visitors</span> in as little as 90 days, SEO is <span class="italic">best utilized</span> only when your business is <span class="italic">currently</span> generating <span class="italic">consistent</span> leads and sales.</p>
    </div>
</div>