<!-- Informational Section -->
<div class="bg-grey-lightest flex justify-center py-24 px-12">
    <div class="flex flex-col justify-start max-w-lg leading-loose md:leading-normal md:text-left">

        <!-- First Section -->
        <h1 class="text-4xl lg:text-5xl font-normal leading-normal py-12 text-center" id="start-here">We Make Marketing Simple</h1>

        <p class="text-grey-darkest text-base md:text-xl my-2">Have you ever felt like you <span class="italic">wasted</span> time and money on marketing (or marketing companies) that haven’t provided results?</p>

        <p class="text-grey-darkest text-base md:text-xl my-2">We believe that business owners like <span class="italic">yourself</span> can’t waste time dealing with <span class="italic">bad marketing</span>, you already have tons of stuff do.</p>

        <p class="text-grey-darkest text-base md:text-xl my-2">You have your businesses to run, you have employees and a ton of other bills to pay and you also have to make a profit. <span class="italic">Right?</span></p>

        <p class="text-grey-darkest text-base md:text-xl my-2">So it’s important that you understand that the <span class="custom-underline italic">#1 reason</span> marketing fails is simply because the <span class="italic">wrong</span> strategy is being used at the <span class="italic">wrong</span> time.</p>

        <p class="text-grey-darkest text-base md:text-xl my-2">If you mess up this <span class="custom-underline italic font-extrabold">one concept</span> you are messing it <span class="italic">all</span> up. Make sense?</p>

        <p class="text-grey-darkest text-base md:text-xl my-2">We keep marketing <span class="italic">simple</span>.</p>

        <p class="text-grey-darkest text-base md:text-xl my-2">And here it all starts with one question...</p>

        <p class="text-grey-darkest text-base md:text-xl my-2">Do you need <span class="custom-underline italic">customers</span> <span class="font-extrabold">NOW</span> or <span class="custom-underline italic">customers</span> <span class="font-extrabold">LATER?</span></p>

        <!-- Second Section -->
        <h2 class="text-2xl lg:text-4xl text-left leading-normal font-bold md:font-normal mt-8 mb-4">Customers Now & Customers Later Marketing</h2>

        <p class="text-grey-darkest text-base md:text-xl my-2">Successful businesses use <span class="italic">short-term</span> and <span class="italic">long-term</span> marketing strategies to make money, survive, grow, and profit.</p>

        <p class="text-grey-darkest text-base md:text-xl my-2">Short-term marketing strategies generate leads <span class="italic custom-underline">quickly</span> so you can make sales and get consistent cash flow to cover expenses and invest <span class="italic">back</span> into your business.</p>

        <p class="text-grey-darkest text-base md:text-xl my-2">Long-term marketing strategies provide a system to give you <span class="italic">fresh</span> leads and prospects at <span class="italic custom-underline">lower</span> costs to <span class="font-extrabold">maximize</span> profits and business growth.</p>

        <p class="text-grey-darkest text-base md:text-xl my-2">We'll match you with the <span class="italic">best</span> strategies to get you leads, clients, and customers when you <span class="italic">need</span> them.</p>

        <p class="text-grey-darkest text-base md:text-xl my-2">Then we do what <span class="font-extrabold">we do best</span> and get you measurable, <span class="italic custom-underline">real results</span> that actually give you a <span class="italic">return</span> on your investment.</p>
    </div>
</div>