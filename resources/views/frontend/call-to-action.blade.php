<!-- Hero Section -->
<div class="bg-grey-lightest flex justify-center py-32 px-12 background-section">
    <div class="container flex flex-wrap justify-between items-center md:mx-8">
        <h1 class="font-normal text-4xl md:text-5xl pr-8 leading-tight">Want More<br> Customers And Sales?</h1>

        <div class="">
            <p class="text-xl md:text-2xl mb-4 text-indigo-darker">We Help Small Businesses Generate More<br> Leads and Sales Using Digital Marketing</p>

            <a class="bg-indigo-dark no-underline rounded shadow py-2 px-10 md:px-32 text-blue-lightest text-lg hover:bg-indigo" href="#start-here">Start Here To Learn More</a>
        </div>
    </div>
</div>