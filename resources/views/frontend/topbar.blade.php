<!-- Navigation Bar -->
<nav class="py-6 px-12 bg-white border-t-4 border-indigo-dark">
    <div class="flex flex-wrap items-center justify-between">
        <img class="w-16 h-16 mr-6 mb-4 lg:mb-0 logo-head" src="{{ $logo }}" alt="Meraki Nine Marketing Logo">

        <div class="flex items-center">
            <div class="">
                <p class="text-right text-base lg:text-lg">“{{ $quote }}”</p>

                <p class="text-right text-xs lg:text-sm text-indigo-light">{{ $quote_author }}</p>
            </div>
        </div>
    </div>
</nav>